package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbDataset

import javax.xml.crypto.Data

@Canonical
class DatasetJson implements TextFileTemplate {

  DbDataset dataset

  @Override
  String getSource() {
    """\
    |{
    |  "comment": "${dataset.name} -- ${regenerationMark}",
    ${dataset.recordsByTable.collect{ table, records -> """\
    |  "${table.identifier.lowerCamelized.get()}Entities": [
    ${records.groupBy{ it.seqNum.get() }.collect { seqNum, record -> """\
    |    {${record.collect{"\"${it.column.identifier.lowerCamelized.get()}\":${it.jsonValue}"}.join(', ')}}
    """.trim()}.join(',\n')}
    |  ]
    """.trim()}.join(',\n')}
    |}
    """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    "${dataset.datasetName}.json"
  }
}
