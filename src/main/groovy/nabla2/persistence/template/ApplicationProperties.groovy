package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.gradle.GenerateDataLoader
import nabla2.persistence.gradle.GenerateDataLoaderOptionSet
import nabla2.persistence.model.Project

@Canonical
class ApplicationProperties implements TextFileTemplate {

  Project project

  GenerateDataLoaderOptionSet options

  @Override
  String getSource() {
    """\
    |# ${project.name}設定ファイル
    |# ${regenerationMark}
    |spring.datasource.url=${dbConnectionUrl}
    |spring.datasource.username=${dbUserName}
    |spring.datasource.password=${dbUserPassword}
    |spring.jpa.hibernate.ddl-auto=create
    ${dbName == 'h2' ? """\
    |spring.datasource.driverClassName=org.h2.Driver
    """ : ''}
    """.replaceAll(/\n(\s*\n)+/, '\n').stripMargin().trim()
  }

  String getDbName() {
    def dbName = options.testDbGenerationVariables.getOrDefault('DB製品名', '').toLowerCase()
    if (['h2', 'postgres'].contains(dbName)) {
      return dbName
    }
    throw new IllegalArgumentException("Unsupported database: ${dbName}")
  }

  String getDbConnectionUrl() {
    def url = options.testDbGenerationVariables.getOrDefault('JDBC接続先', '')
    if (!url) throw new IllegalArgumentException(
      "JDBC connection URL must not be blank"
    )
    url
  }

  String getDbUserName() {
    options.testDbGenerationVariables.getOrDefault('DBユーザ名', '')
  }

  String getDbUserPassword() {
    options.testDbGenerationVariables.getOrDefault('DBユーザパスワード', '')
  }

  @Override
  String getRelPath() {
    "application.properties"
  }
}
