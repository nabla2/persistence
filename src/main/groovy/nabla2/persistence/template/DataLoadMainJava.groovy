package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.gradle.GenerateDataLoaderOptionSet
import nabla2.persistence.model.DbDataset
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.Project

@Canonical
class DataLoadMainJava implements TextFileTemplate {

  Project project

  GenerateDataLoaderOptionSet options

  @Override
  String getSource() {
    """\
    |package ${project.namespace};
    |
    |import java.util.List;
    |import org.springframework.boot.CommandLineRunner;
    |import org.springframework.boot.SpringApplication;
    |import org.springframework.boot.autoconfigure.SpringBootApplication;
    |import org.springframework.context.annotation.Bean;
    |import com.fasterxml.jackson.databind.ObjectMapper;
    ${dataset.tables.collect{ table -> """\
    |import ${table.namespace}.${table.entityClassName};
    |import ${table.namespace}.${table.repositoryClassName};
    """.trim()}.join('\n')}
    |
    |/**
    | * データロードコマンドラインツール
    | * ${regenerationMark}
    | */
    |@SpringBootApplication
    |public class ${className} {
    |
    |    public static void main(String... args) {
    |        SpringApplication.run(${className}.class);
    |    }
    |
    ${project.datasets.collect{ dataset -> """\
    |    /**
    |     * ${dataset.name}のJSONデータ形式
    |     */
    |    static class ${structClassName(dataset)} {
    |        public String comment;
    ${dataset.tables.collect{ table -> """\
    |        public List<${table.entityClassName}> ${tableLName(table)}Entities;
    """.trim()}.join('\n')} 
    |    }
    """.trim()}.join('\n')}
    |
    |    /**
    |     * データをロードする
    ${dataset.tables.collect{ table -> """\
    |     * @param ${repositoryLName(table)} ${table.name}リポジトリ
    """.trim()}.join('\n')}
    |     * @return ロード処理
    |     */
    |    @Bean
    |    public CommandLineRunner run(${dataset.tables.collect{"${it.repositoryClassName} ${repositoryLName(it)}"}.join(',\n|' + ' '*33)}) {
    |        return (args) -> {
    |            ${structClassName(dataset)} ${datasetLName(dataset)} = new ObjectMapper().readValue(
    |                getClass().getResourceAsStream("/dataset/${dataset.datasetName}.json"),
    |                ${structClassName(dataset)}.class
    |            );
    ${dataset.tables.collect{ table -> """\
    |            ${datasetLName(dataset)}.${tableLName(table)}Entities.forEach(${repositoryLName(table)}::save);
    """.trim()}.join('\n')}
    |        };
    |    }
    |}
    """.trim().stripMargin()
  }

  DbDataset getDataset() {
    def dataSetName = options.testDbGenerationVariables.getOrDefault('テストデータ断面名', '')
    def found = project.datasets.find{ it.name.sameAs(dataSetName) }
    if (!found) throw new IllegalArgumentException(
      "Unkown dataset name: ${dataSetName}"
    )
    found
  }

  static String structClassName(DbDataset dataset) {
    dataset.identifier.upperCamelized.get() + 'Structure'
  }

  static String repositoryLName(DbTable table) {
    table.repositoryClassName.with{it[0].toLowerCase()+it[1..-1]}
  }

  static String tableLName(DbTable table) {
    table.identifier.lowerCamelized.get()
  }

  static String datasetLName(DbDataset dataset) {
    dataset.identifier.lowerCamelized.get()
  }

  static String getClassName() {
    "DataLoadMain"
  }

  @Override
  String getRelPath() {
    "${project.namespace}.${className}".replace('.', '/') + ".java"
  }
}
