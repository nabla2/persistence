package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbAccessMethod

@Canonical
class MirageSqlCustomQuerySql implements TextFileTemplate {

  DbAccessMethod method

  @Override
  String getSource() {
    """\
    |-- ${method.name}
    |-- ${regenerationMark}
    |${method.query.replaceAll(/\n/, '\n|')}
    """.replaceAll(/\n\s*\n/, '\n').trim().stripMargin()
  }

  String getClassName() {
    "${method.mainTable.repositoryClassName}Impl"
  }

  @Override
  String getRelPath() {
    "${method.mainTable.namespace.get()}.${className}.${method.methodId}".replace('.', '/') + '.sql'
  }

  @Override
  boolean needsRegenerated(File file) {
    if (!method.mirageStatement) return false
    TextFileTemplate.super.needsRegenerated(file)
  }
}
