package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.gradle.GenerateDataLoaderOptionSet
import nabla2.persistence.model.Project

@Canonical
class BuildGradle implements TextFileTemplate {

  Project project

  GenerateDataLoaderOptionSet options

  @Override
  String getSource() {
    """\
    |/** 
    | * ${project.name}永続化データ管理ツール 
    | * ${regenerationMark}
    | */
    |buildscript {
    |  dependencies {
    |    classpath 'org.springframework.boot:spring-boot-gradle-plugin:1.+'
    |  }
    |  repositories {
    |    jcenter()
    |  }
    |}
    |
    |apply plugin: 'java'
    |apply plugin: 'maven-publish'
    |apply plugin: 'org.springframework.boot'
    |
    |bootRun {
    |  addResources = true
    |}
    |
    |repositories {
    |  jcenter()
    |}
    |
    |dependencies {
    |  compile 'org.springframework.boot:spring-boot-starter-data-jpa'
    |  compile 'org.apache.commons:commons-lang3:3.+'
    |  compile 'com.fasterxml.jackson.core:jackson-databind:2.+'
    |  compile 'com.miragesql:miragesql:2.+'
    |  compile 'com.h2database:h2'
    ${dbName == 'postgres' ? """\
    |  compile 'org.postgresql:postgresql:42.+'
    """ : ''}
    |}
    """.replaceAll(/\n(\s*\n)+/, '\n').stripMargin().trim()
  }

  String getDbName() {
    def dbName = options.testDbGenerationVariables.getOrDefault('DB製品名', '').toLowerCase()
    if (['h2', 'postgres'].contains(dbName)) {
      return dbName
    }
    throw new IllegalArgumentException("Unsupported database: ${dbName}")
  }

  @Override
  String getRelPath() {
    'build.gradle'
  }
}
