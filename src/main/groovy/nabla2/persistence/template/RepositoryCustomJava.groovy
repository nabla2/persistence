package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbTable

@Canonical
class RepositoryCustomJava implements TextFileTemplate {

  DbTable table

  @Override
  String getSource() {
    """\
    |package ${table.namespace};
    |import javax.persistence.Tuple;
    |import java.util.List;
    | 
    |/**
    | * ${table.name}リポジトリ追加API定義
    | * ${regenerationMark}
    | */
    |public interface ${className} {
    |
    ${table.customMethods.collect{ access -> """\
    |    /**
    |     * ${access.name}
    ${access.params.collect{ param -> """\
    |     * @param ${param.key} ${param.name}
    """.trim()}.join('\n')}
    |     * @return 処理結果
    |     */
    |    ${access.javaReturnType.type} ${access.methodId}(${access.paramSignature});
    """.trim()}.join('\n')}
    |}
    """.replaceAll(/\n\s*\n/, '\n').trim().stripMargin()
  }

  String getClassName() {
    "${table.repositoryClassName}Custom"
  }

  @Override
  String getRelPath() {
    "${table.namespace.get().replace('.', '/')}/${className}.java"
  }

  @Override
  boolean needsRegenerated(File file) {
    if (table.customMethods.empty) false
    TextFileTemplate.super.needsRegenerated(file)
  }
}
