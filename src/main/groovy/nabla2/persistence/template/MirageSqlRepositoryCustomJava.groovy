package nabla2.persistence.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbTable
import nabla2.persistence.trait.DbAccessMethodReturnType

@Canonical
class MirageSqlRepositoryCustomJava implements TextFileTemplate {

  DbTable table

  @Override
  String getSource() {
    """\
    |package ${table.namespace};
    |import java.util.*;
    |import javax.annotation.PostConstruct;
    |import javax.persistence.Tuple;
    |import org.springframework.stereotype.Repository;
    |import ${table.project.namespace}.${MirageSqlRepositoryBaseJava.className};
    | 
    |/**
    | * ${table.name}リポジトリ追加API実装
    | * ${regenerationMark}
    | */
    |@Repository
    |public class ${className} extends ${MirageSqlRepositoryBaseJava.className} implements ${interfaceName} {
    |
    |    @PostConstruct
    |    public void init() {
    ${withMirage().collect{ access -> """\
    |        loadStatement("${access.methodId}");
    """.trim()}.join('\n')}
    |    }
    |
    ${withMirage().collect{ access -> """\
    |    /**
    |     * ${access.name}
    ${access.params.collect{ param -> """\
    |     * @param ${param.key} ${param.name}
    """.trim()}.join('\n')}
    |     * @return 処理結果
    |     */
    |    @Override
    |    public ${access.javaReturnType.type} ${access.methodId}(${access.paramSignature}) {
    |        Map<String, Object> params = new HashMap<>();
    ${access.params.collect{ param -> """\
    |        params.put("${param.key}", ${param.key});
    """.trim()}.join('\n')}
    ${(queryMethodNameFor(access) == 'execute') ? """\
    |        return execute("${access.methodId}", params);
    """ : """\
    |        return ${queryMethodNameFor(access)}(${recordTypeOf(access)}.class, "${access.methodId}", params);
    """}
    |    }
    """.trim()}.join('\n')}
    |}
    """.replaceAll(/\n\s*\n/, '\n').trim().stripMargin()
  }

  @Memoized
  static String queryMethodNameFor(DbAccessMethod access) {
    if (access.query.matches(/^[\s\S]*(DELETE|delete|UPDATE|update|INSERT|insert)[\n\s(][\s\S]*$/)) {
      return 'execute'
    }
    access.returnsList() ? 'find' : 'findOne'
  }

  static String recordTypeOf(DbAccessMethod access) {
    String returnType = access.javaReturnType.type
    access.returnsList() ? returnType.replaceAll(/^List<|>$/, '') : returnType
  }

  List<DbAccessMethod> withMirage() {
    table.accessMethods.findAll{ it.mirageStatement }
  }

  String getInterfaceName() {
    "${table.repositoryClassName}Custom"
  }

  String getClassName() {
    "${table.repositoryClassName}Impl"
  }

  @Override
  String getRelPath() {
    "${table.namespace.get().replace('.', '/')}/${className}.java"
  }

  @Override
  boolean needsRegenerated(File file) {
    if (withMirage().empty) return false
    TextFileTemplate.super.needsRegenerated(file)
  }
}
