package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbTable

@Canonical
class JpaEntityClassJava implements TextFileTemplate {

  DbTable table

  @Override
  String getSource() {
    """\
    |package ${table.namespace};
    |
    |import org.apache.commons.lang3.builder.EqualsBuilder;
    |import org.apache.commons.lang3.builder.HashCodeBuilder;
    |import org.apache.commons.lang3.builder.ToStringBuilder;
    |
    |import java.util.Date;
    |import javax.persistence.*;
    |import java.io.Serializable;
    |
    |/**
    | * ${table.name}エンティティ
    | * ${regenerationMark}
    | */
    |@Entity(name="${table.tableName}")
    ${table.hasCompositeKey() ? """\
    |@IdClass(${table.entityClassName}.PK.class)
    """.trim() : ''}
    |public class ${table.entityClassName} {
    ${table.columns.collect{ column ->
      String lname = column.identifier.lowerCamelized.get()  
      String uname = column.identifier.upperCamelized.get()  
      String type = column.javaDataType
    """\
    |    /**
    |     * ${column.name}
    |     */
    ${column.isPk.truthy ? """\
    |    @Id
    """.trim() : ''}
    ${column.isDerived.truthy ? """\
    |    @Transient
    """.trim() : ''}
    |    private ${type} ${lname};
    |    public ${type} get${uname}() {
    |        return ${lname};
    |    }
    |    public void set${column.identifier.upperCamelized.get()}(${type} ${lname}) {
    |        this.${lname} = ${lname};
    |    }
    |
    """.trim()}.join('\n')}
    ${table.hasCompositeKey() ? """\
    |    /**
    |     * 主キークラス
    |     */
    |    public static class PK implements Serializable {
    ${table.pkColumns.collect{ column -> """\
    |        public ${column.javaDataType} ${column.identifier.lowerCamelized.get()};
    """.trim()}.join('\n')}
    |        @Override
    |        public int hashCode() {
    |           return HashCodeBuilder.reflectionHashCode(this);
    |        }
    |        @Override
    |        public boolean equals(Object another) {
    |            return EqualsBuilder.reflectionEquals(this, another);
    |        }
    |    }
    """.trim() : ''}
    |
    |    @Override
    |    public boolean equals(Object another) {
    |        return EqualsBuilder.reflectionEquals(this, another);
    |    }
    |
    |    @Override
    |    public int hashCode() {
    |        return HashCodeBuilder.reflectionHashCode(this);
    |    }
    |
    |    @Override
    |    public String toString() {
    |        return ToStringBuilder.reflectionToString(this);
    |    }
    |}
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }

  @Override
  String getRelPath() {
    return "${table.namespace.get().replace('.', '/')}/${table.entityClassName}.java"
  }
}
