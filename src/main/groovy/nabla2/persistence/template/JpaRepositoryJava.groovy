package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbTable

@Canonical
class JpaRepositoryJava implements TextFileTemplate {

  DbTable table

  @Override
  String getSource() {
    """\
    |package ${table.namespace};
    |import org.springframework.data.jpa.repository.JpaRepository;
    |import org.springframework.data.jpa.repository.Query;
    |import org.springframework.data.repository.query.Param;
    |import javax.persistence.Tuple;
    |import java.util.List;
    | 
    |/**
    | * ${table.name}リポジトリ
    | * ${regenerationMark}
    | */
    |public interface ${table.repositoryClassName} extends ${table.repositoryClassName}Custom, JpaRepository<${table.entityClassName}, ${table.idType}> {
    |
    ${table.accessMethods.findAll{it.jpaBacked}.collect{ access -> """\
    |    /**
    |     * ${access.name}
    ${access.params.collect{ param -> """\
    |     * @param ${param.key} ${param.name}
    """.trim()}.join('\n')}
    |     * @return 処理結果
    |     */
    ${access.scriptType.sameAs("JPQL") ? """\
    |    @Query("${access.query.replaceAll(/\n/, '\\\\n"\n|         + "')}")
    """.trim() : ''}
    |    ${access.javaReturnType.type} ${access.methodId}(${access.paramSignature});
    |
    """.trim()}.join('\n')}
    |}
    """.replaceAll(/\n\s*\n/, '\n').trim().stripMargin()
  }

  @Override
  String getRelPath() {
    "${table.namespace.get().replace('.', '/')}/${table.repositoryClassName}.java"
  }
}
