package nabla2.persistence.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbTable

@Canonical
class MirageSqlRepositoryBaseJava implements TextFileTemplate {

  DbTable table

  @Override
  String getSource() {
    """\
    |package ${table.project.namespace};
    |
    |import com.miragesql.miragesql.SqlManager;
    |import com.miragesql.miragesql.SqlResource;
    |import com.miragesql.miragesql.StringSqlResource;
    |import org.springframework.beans.factory.annotation.Autowired;
    |import org.springframework.core.io.Resource;
    |import org.springframework.core.io.ResourceLoader;
    |
    |import java.io.IOException;
    |import java.nio.file.Files;
    |import java.util.HashMap;
    |import java.util.List;
    |import java.util.Map;
    |
    |/**
    | * Mirage-SQLによるリポジトリの共通実装を与える。
    | * ${regenerationMark}
    | */
    |public class ${className} {
    |
    |    @Autowired
    |    private SqlManager sql;
    |
    |    @Autowired
    |    private ResourceLoader resourceLoader;
    |
    |    private final Map<String, String> statements = new HashMap<>();
    |
    |    /**
    |     * 指定されたキー名に対応するSQLファイルからSQLスクリプトを読み込んでキャッシュする。
    |     *
    |     * この実装はスレッドセーフでない。
    |     * static initializerもしくはシングルトンであるSpringManaged Beanの
    |     * <code>@PostConstruct</code>などから呼ばれることを想定している。
    |     *
    |     * @param name SQL名
    |     */
    |    protected void loadStatement(String name) {
    |        statements.put(name, getStatement(name));
    |    }
    |
    |    private String getStatement(String name) {
    |        String prefix = "classpath:" + getClass().getCanonicalName().replace('.', '/') + "/";
    |        String suffix = ".sql";
    |        String filename = prefix + name + suffix;
    |        Resource resource = resourceLoader.getResource(filename);
    |        try {
    |            return new String(Files.readAllBytes(resource.getFile().toPath()));
    |        } catch (IOException e) {
    |            throw new RuntimeException("SQL template not found: " + filename);
    |        }
    |    }
    |
    |    private SqlResource sqlResource(String name) {
    |        return new StringSqlResource(statements.get(name));
    |    }
    |
    |    /**
    |     * 指定されたクエリを実行し、その結果リストを返す。
    |     *
    |     * @param clazz レコードの型
    |     * @param name SQL名
    |     * @param params SQL文のバインドパラメータ
    |     * @param <TRecord> レコードの型
    |     * @return 結果リスト
    |     */
    |    protected <TRecord> List<TRecord> find(Class<TRecord> clazz, String name, Map<String, ?> params) {
    |        return sql.getResultList(
    |            clazz,
    |            sqlResource(name),
    |            params
    |        );
    |    }
    |
    |    /**
    |     * 指定されたクエリを実行し、対象レコードを1件返す。
    |     * 存在しない場合はnullを返す。
    |     *
    |     * なお、このメソッドではクライアントカーソルで結果セットを全て読み込んでしまう。
    |     * 複数件の結果セットから先頭行を取得する場合は、SQLでLIMIT句を挿入するなどして
    |     * DB側で結果セット絞り込むことを想定している。
    |     *
    |     * @param clazz レコードの型
    |     * @param name SQL名
    |     * @param params SQL文のバインドパラメータ
    |     * @param <TRecord> レコードの型
    |     * @return 結果レコード(対象レコードが存在しない場合はnull)
    |     */
    |    protected <TRecord> TRecord findOne(Class<TRecord> clazz, String name, Map<String, ?> params) {
    |        List<TRecord> results = sql.getResultList(
    |            clazz,
    |            sqlResource(name),
    |            params
    |        );
    |        return results.isEmpty()
    |             ? null
    |             : results.get(0);
    |    }
    |
    |    /**
    |     * 指定されたSQLを実行する。
    |     * @param name SQL名
    |     * @param params SQL文のバインドパラメータ
    |     * @return SQLの実行により影響を受けたレコード件数
    |     */
    |    protected Long execute(String name, Map<String, ?> params) {
    |        return (long) sql.executeUpdate(
    |            sqlResource(name),
    |            params
    |        );
    |    }
    |}
    """.replaceAll(/\n\s*\n/, '\n\n').trim().stripMargin()
  }

  static String getClassName() {
    'MirageRepositoryImplBase'
  }

  @Override
  String getRelPath() {
    "${table.project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
