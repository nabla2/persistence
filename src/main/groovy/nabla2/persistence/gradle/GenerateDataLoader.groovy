package nabla2.persistence.gradle

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbAccessMethodValidator
import nabla2.persistence.model.DbColumnValidator
import nabla2.persistence.model.DbDataset
import nabla2.persistence.model.DbDatasetValidator
import nabla2.persistence.model.DbRecordValidator
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.DbTableValidator
import nabla2.persistence.model.Project
import nabla2.persistence.model.ProjectValidator
import nabla2.persistence.template.ApplicationProperties
import nabla2.persistence.template.BuildGradle
import nabla2.persistence.template.DataLoadMainJava
import nabla2.persistence.template.DatasetJson
import nabla2.persistence.template.JpaEntityClassJava
import nabla2.persistence.template.JpaRepositoryJava
import nabla2.persistence.template.MirageSqlCustomQuerySql
import nabla2.persistence.template.MirageSqlRepositoryBaseJava
import nabla2.persistence.template.MirageSqlRepositoryCustomJava
import nabla2.persistence.template.MirageSqlSettingJava
import nabla2.persistence.template.RepositoryCustomJava
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * データロードツールを生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateDataLoader extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File applicationPropertiesDir
  @OutputDirectory @Optional File mirageSqlSettingJavaDir
  @OutputDirectory @Optional File dataLoadMainJavaDir
  @OutputDirectory @Optional File jpaEntityClassJavaDir
  @OutputDirectory @Optional File jpaRepositoryJavaDir
  @OutputDirectory @Optional File repositoryCustomJavaDir
  @OutputDirectory @Optional File mirageSqlRepositoryCustomJavaDir
  @OutputDirectory @Optional File mirageSqlRepositoryBaseJavaDir
  @OutputDirectory @Optional File mirageSqlCustomQuerySqlDir
  @OutputDirectory @Optional File datasetJsonDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'データロードツールを生成'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.get()
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('generate_data_loader')
      }.toList().blockingGet()
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new BuildGradle(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new ApplicationProperties(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new MirageSqlSettingJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new DataLoadMainJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new JpaEntityClassJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new JpaRepositoryJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new RepositoryCustomJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new MirageSqlRepositoryCustomJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .take(1)
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new MirageSqlRepositoryBaseJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbAccessMethod.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new MirageSqlCustomQuerySql(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbDataset.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateDataLoaderOptionSet(optionList, project.properties)
        new DatasetJson(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateDataLoaderOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources/dataset'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new DbTableValidator().validate(table$),
      new DbColumnValidator().validate(table$),
      new DbAccessMethodValidator().validate(table$),
      new DbDatasetValidator().validate(table$),
      new DbRecordValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateDataLoader> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateDataLoader task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.applicationPropertiesDir = new File(baseDir, 'src/main/resources')
      task.mirageSqlSettingJavaDir = new File(baseDir, 'src/main/java')
      task.dataLoadMainJavaDir = new File(baseDir, 'src/main/java')
      task.jpaEntityClassJavaDir = new File(baseDir, 'src/main/java')
      task.jpaRepositoryJavaDir = new File(baseDir, 'src/main/java')
      task.repositoryCustomJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlRepositoryCustomJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlRepositoryBaseJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlCustomQuerySqlDir = new File(baseDir, 'src/main/resources')
      task.datasetJsonDir = new File(baseDir, 'src/main/resources/dataset')
    }
  }
}