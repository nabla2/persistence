package nabla2.persistence

import groovy.transform.Canonical
import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbAccessMethodValidator
import nabla2.persistence.model.DbColumn
import nabla2.persistence.model.DbColumnValidator
import nabla2.persistence.model.DbDataset
import nabla2.persistence.model.DbDatasetValidator
import nabla2.persistence.model.DbRecord
import nabla2.persistence.model.DbRecordValidator
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.DbTableValidator
import nabla2.persistence.model.Project
import nabla2.persistence.model.ProjectValidator

/**
 * Persistenceエンティティローダ
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class PersistenceEntityLoader {

  final Observable<Table> table$

  private PersistenceEntityLoader(File file) {
    table$ = ExcelWorkbookLoader.from(file).table$
  }

  private PersistenceEntityLoader(InputStream stream) {
    table$ = ExcelWorkbookLoader.from(stream).table$
  }

  static PersistenceEntityLoader from(File file) {
    new PersistenceEntityLoader(file)
  }

  static PersistenceEntityLoader from(InputStream stream) {
    new PersistenceEntityLoader(stream)
  }

  Observable<Project> getProject$() {
    Project.from(table$)
  }
  Observable<DbTable> getDbTable$() {
    DbTable.from(table$)
  }
  Observable<DbColumn> getDbColumn$() {
    DbColumn.from(table$)
  }
  Observable<DbAccessMethod> getDbAccessMethod$() {
    DbAccessMethod.from(table$)
  }
  Observable<DbDataset> getDbDataset$() {
    DbDataset.from(table$)
  }
  Observable<DbRecord> getDbRecord$() {
    DbRecord.from(table$)
  }

  Observable<ConstraintViolation<?>> getConstraintViolation$() {
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new DbTableValidator().validate(table$),
      new DbColumnValidator().validate(table$),
      new DbAccessMethodValidator().validate(table$),
      new DbDatasetValidator().validate(table$),
      new DbRecordValidator().validate(table$),
    ).flatMap{ it }
  }

}