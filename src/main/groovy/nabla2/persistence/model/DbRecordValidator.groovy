package nabla2.persistence.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * DBレコードバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class DbRecordValidator implements Validator<DbRecord> {
  Observable<ConstraintViolation<DbRecord>> validate(Observable<Table> table$) {
    Observable<DbRecord> entity$ = DbRecord.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<DbRecord>> validateUniqueConstraint(Observable<DbRecord> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        datasetName : entity.datasetName.value.map{it.toString()}.orElse(null),
        tableName : entity.tableName.value.map{it.toString()}.orElse(null),
        columnName : entity.columnName.value.map{it.toString()}.orElse(null),
        seqNum : entity.seqNum.value.map{it.toString()}.orElse(null),
      ])
      DbRecord another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<DbRecord> duplicated = it.get('__duplicated__')
      new ConstraintViolation<DbRecord>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, データセット名, テーブル名, カラム名, 通番',
          'DBレコード',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<DbRecord>> validateCardinalityConstraint(Observable<DbRecord> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.table == null) {
        violations.add(new ConstraintViolation<DbRecord>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'DBテーブル',
            "プロジェクト名=${entity.projectName}, 名称=${entity.tableName}",
            entity,
          )
        ))
      }
      if (entity.column == null) {
        violations.add(new ConstraintViolation<DbRecord>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'DBカラム',
            "プロジェクト名=${entity.projectName}, テーブル名=${entity.tableName}, 名称=${entity.columnName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DbRecord>> validatePropertyType(Observable<DbRecord> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.datasetName.messageIfInvalid.map{['データセット名', it]}.orElse(null),
        entity.tableName.messageIfInvalid.map{['テーブル名', it]}.orElse(null),
        entity.columnName.messageIfInvalid.map{['カラム名', it]}.orElse(null),
        entity.seqNum.messageIfInvalid.map{['通番', it]}.orElse(null),
        entity.columnValue.messageIfInvalid.map{['値', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<DbRecord>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DbRecord>> validateRequiredProperty(Observable<DbRecord> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.datasetName.literal.map{''}.orElse('データセット名'),
        entity.tableName.literal.map{''}.orElse('テーブル名'),
        entity.columnName.literal.map{''}.orElse('カラム名'),
        entity.seqNum.literal.map{''}.orElse('通番'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<DbRecord>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}