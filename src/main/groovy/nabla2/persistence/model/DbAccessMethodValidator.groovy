package nabla2.persistence.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * DBアクセスメソッドバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class DbAccessMethodValidator implements Validator<DbAccessMethod> {
  Observable<ConstraintViolation<DbAccessMethod>> validate(Observable<Table> table$) {
    Observable<DbAccessMethod> entity$ = DbAccessMethod.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<DbAccessMethod>> validateUniqueConstraint(Observable<DbAccessMethod> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        mainTableName : entity.mainTableName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      DbAccessMethod another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<DbAccessMethod> duplicated = it.get('__duplicated__')
      new ConstraintViolation<DbAccessMethod>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 主管テーブル名, 名称',
          'DBアクセスメソッド',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<DbAccessMethod>> validateCardinalityConstraint(Observable<DbAccessMethod> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.mainTable == null) {
        violations.add(new ConstraintViolation<DbAccessMethod>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'DBテーブル',
            "プロジェクト名=${entity.projectName}, 名称=${entity.mainTableName}",
            entity,
          )
        ))
      }
      if (entity.project == null) {
        violations.add(new ConstraintViolation<DbAccessMethod>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DbAccessMethod>> validatePropertyType(Observable<DbAccessMethod> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.mainTableName.messageIfInvalid.map{['主管テーブル名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.methodName.messageIfInvalid.map{['メソッド名', it]}.orElse(null),
        entity.returnType.messageIfInvalid.map{['データ型', it]}.orElse(null),
        entity.recordContent.messageIfInvalid.map{['レコード内容', it]}.orElse(null),
        entity.scriptType.messageIfInvalid.map{['スクリプトタイプ', it]}.orElse(null),
        entity.script.messageIfInvalid.map{['スクリプト', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<DbAccessMethod>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DbAccessMethod>> validateRequiredProperty(Observable<DbAccessMethod> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.mainTableName.literal.map{''}.orElse('主管テーブル名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.methodName.literal.map{''}.orElse('メソッド名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<DbAccessMethod>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}