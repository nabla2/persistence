package nabla2.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.persistence.trait.DbColumnTrait
import nabla2.persistence.model.DbTable
/**
 * DBカラム
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class DbColumn implements DbColumnTrait<DbColumn> {

  /** 論理名 */
  static final String ENTITY_NAME = "DBカラム"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * テーブル名
   */
  SingleLineText tableName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * データ型
   */
  StrictJavaIdentifier dataType
  /**
   * データ長
   */
  SingleLineText dataSize
  /**
   * データ精度
   */
  SingleLineText dataPrecision
  /**
   * 主キー項目
   */
  BooleanFlag isPk
  /**
   * 導出値
   */
  BooleanFlag isDerived
  /**
   * 自動採番
   */
  BooleanFlag isGenerated
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * テーブル
   */
  @Memoized
  @JsonIgnore
  DbTable getTable() {
    DbTable.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.tableName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<DbColumn> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new DbColumn(
            projectName : new SingleLineText(row['プロジェクト名']),
            tableName : new SingleLineText(row['テーブル名']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            dataType : new StrictJavaIdentifier(row['データ型']),
            dataSize : new SingleLineText(row['データ長']),
            dataPrecision : new SingleLineText(row['データ精度']),
            isPk : new BooleanFlag(row['主キー項目']),
            isDerived : new BooleanFlag(row['導出値']),
            isGenerated : new BooleanFlag(row['自動採番']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}