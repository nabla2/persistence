package nabla2.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.persistence.trait.DbAccessMethodTrait
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.Project
/**
 * DBアクセスメソッド
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class DbAccessMethod implements DbAccessMethodTrait<DbAccessMethod> {

  /** 論理名 */
  static final String ENTITY_NAME = "DBアクセスメソッド"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 主管テーブル名
   */
  SingleLineText mainTableName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * メソッド名
   */
  ParameterizedText methodName
  /**
   * データ型
   */
  SingleLineText returnType
  /**
   * レコード内容
   */
  MultiLineText recordContent
  /**
   * スクリプトタイプ
   */
  SingleLineText scriptType
  /**
   * スクリプト
   */
  ParameterizedText script

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 主管テーブル
   */
  @Memoized
  @JsonIgnore
  DbTable getMainTable() {
    DbTable.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.mainTableName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<DbAccessMethod> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new DbAccessMethod(
            projectName : new SingleLineText(row['プロジェクト名']),
            mainTableName : new SingleLineText(row['主管テーブル名']),
            name : new SingleLineText(row['名称']),
            methodName : new ParameterizedText(row['メソッド名']),
            returnType : new SingleLineText(row['データ型']),
            recordContent : new MultiLineText(row['レコード内容']),
            scriptType : new SingleLineText(row['スクリプトタイプ'] ?: "SpringData"),
            script : new ParameterizedText(row['スクリプト']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}