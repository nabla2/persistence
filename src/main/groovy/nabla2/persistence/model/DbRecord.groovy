package nabla2.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.OrdinalNumber
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.persistence.trait.DbRecordTrait
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.DbColumn
/**
 * DBレコード
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class DbRecord implements DbRecordTrait<DbRecord> {

  /** 論理名 */
  static final String ENTITY_NAME = "DBレコード"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * データセット名
   */
  SingleLineText datasetName
  /**
   * テーブル名
   */
  SingleLineText tableName
  /**
   * カラム名
   */
  SingleLineText columnName
  /**
   * 通番
   */
  OrdinalNumber seqNum
  /**
   * 値
   */
  MultiLineText columnValue

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * テーブル
   */
  @Memoized
  @JsonIgnore
  DbTable getTable() {
    DbTable.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.tableName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * カラム
   */
  @Memoized
  @JsonIgnore
  DbColumn getColumn() {
    DbColumn.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.tableName.sameAs(it.tableName) &&
      this.columnName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<DbRecord> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new DbRecord(
            projectName : new SingleLineText(row['プロジェクト名']),
            datasetName : new SingleLineText(row['データセット名']),
            tableName : new SingleLineText(row['テーブル名']),
            columnName : new SingleLineText(row['カラム名']),
            seqNum : new OrdinalNumber(row['通番']),
            columnValue : new MultiLineText(row['値']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}