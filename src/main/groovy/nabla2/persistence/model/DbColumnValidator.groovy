package nabla2.persistence.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * DBカラムバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class DbColumnValidator implements Validator<DbColumn> {
  Observable<ConstraintViolation<DbColumn>> validate(Observable<Table> table$) {
    Observable<DbColumn> entity$ = DbColumn.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<DbColumn>> validateUniqueConstraint(Observable<DbColumn> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        tableName : entity.tableName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      DbColumn another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<DbColumn> duplicated = it.get('__duplicated__')
      new ConstraintViolation<DbColumn>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, テーブル名, 名称',
          'DBカラム',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<DbColumn>> validateCardinalityConstraint(Observable<DbColumn> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.table == null) {
        violations.add(new ConstraintViolation<DbColumn>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'DBテーブル',
            "プロジェクト名=${entity.projectName}, 名称=${entity.tableName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DbColumn>> validatePropertyType(Observable<DbColumn> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.tableName.messageIfInvalid.map{['テーブル名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.dataType.messageIfInvalid.map{['データ型', it]}.orElse(null),
        entity.dataSize.messageIfInvalid.map{['データ長', it]}.orElse(null),
        entity.dataPrecision.messageIfInvalid.map{['データ精度', it]}.orElse(null),
        entity.isPk.messageIfInvalid.map{['主キー項目', it]}.orElse(null),
        entity.isDerived.messageIfInvalid.map{['導出値', it]}.orElse(null),
        entity.isGenerated.messageIfInvalid.map{['自動採番', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<DbColumn>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DbColumn>> validateRequiredProperty(Observable<DbColumn> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.tableName.literal.map{''}.orElse('テーブル名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.dataType.literal.map{''}.orElse('データ型'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<DbColumn>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}