package nabla2.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.persistence.trait.DbDatasetTrait
import nabla2.persistence.model.Project
import nabla2.persistence.model.DbRecord
/**
 * DBデータセット
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class DbDataset implements DbDatasetTrait<DbDataset> {

  /** 論理名 */
  static final String ENTITY_NAME = "DBデータセット"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * コメント
   */
  SingleLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * レコードリスト
   */
  @Memoized
  @JsonIgnore
  List<DbRecord> getRecords() {
    DbRecord.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.name.sameAs(it.datasetName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<DbDataset> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new DbDataset(
            projectName : new SingleLineText(row['プロジェクト名']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            comment : new SingleLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}