package nabla2.persistence.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbColumn
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.Project
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

trait DbAccessMethodTrait<T> {
  abstract ParameterizedText getMethodName()
  abstract ParameterizedText getScript()
  abstract DbTable getMainTable()
  abstract Project getProject()
  abstract SingleLineText getReturnType()
  abstract SingleLineText getScriptType()

  @Memoized
  String getQuery() {
    script
    .embedParams{ "${parseParam(it).key}" }.orElseThrow{ new IllegalStateException(
      "SQL statement is blank.\n${this}"
    )}
    .replaceAll(/\/\*+\s*([._a-zA-Z]+)\s*\*+\//) { m ->
      def token = m[1].with { t ->
        RESERVED.any{ t.startsWith(it) } ? t : lowerCamelize(t)
      }
      "/*${token}*/"
    }
  }

  static List<String> RESERVED = ['IF', 'ELSE', 'BEGIN', 'END']

  @Memoized
  String getMethodId() {
    methodName.embedParams{" ${upperCamelize(parseParam(it).key)} "}.get()
              .split(/\s+/)
              .collect{ it[0].toUpperCase() + it[1..-1] }
              .join('')
              .with{ it[0].toLowerCase() + it[1..-1] }
  }

  @Memoized
  DbAccessMethodReturnType getJavaReturnType() {
    DbAccessMethodReturnType.of((DbAccessMethod)this)
  }

  @Memoized
  boolean returnsTuple() {
    javaReturnType.with { it.startsWith('Tuple') || it.contains('<Tuple>') }
  }

  @Memoized
  boolean isJpaBacked() {
    scriptType.value.map{ it.toUpperCase().with{it == 'JPQL' || it == 'SPRINGDATA'} }.orElse(true)
  }

  @Memoized
  boolean isMirageStatement() {
    scriptType.value.map{ it.toUpperCase() == 'MIRAGE' }.orElse(false)
  }

  @Memoized
  boolean returnsList() {
    javaReturnType.list
  }

  @Memoized
  DbAccessMethodParam parseParam(String str) {
    if (str.contains(':')) {
      List<String> tokens = str.tokenize(':')
      return new DbAccessMethodParam(
        name      :tokens[0],
        parentKey :'',
        key       :tokens[1],
        type      :project.tables.find{ it.name.sameAs(tokens[2])}.with {
                     it ? it.entityClassName : tokens[2..-1].join(':')
                   },
      )
    }
    List<String> tokens = str.tokenize('.')
    DbTable table = project.tables.find{ it.name.sameAs(tokens[0]) }
    if (!table) throw new IllegalArgumentException(
      "Unknown table name: ${tokens[0]}\n${this}"
    )
    if (tokens.size() == 1) {
      return new DbAccessMethodParam(
        name      :table.name.get(),
        parentKey :'',
        key       :table.tableName,
        type      :table.entityClassName,
      )
    }
    DbColumn column = table.columns.find{ it.name.sameAs(tokens.last()) }
    if (!column) throw new IllegalArgumentException(
      "Unknown column name: ${tokens.last()} in the table: ${table.name}"
    )
    return new DbAccessMethodParam(
      name      :"${table.name}.${column.name}",
      parentKey :table.tableName,
      key       :column.identifier.get(),
      type      :column.javaDataType,
    )
  }

  @Memoized
  String getParamSignature() {
    params.collect{
      "${scriptType.sameAs('JPQL') ? "@Param(\"${it.key}\") " : ''}${it.type} ${it.key}"
    }.join(', ')
  }

  @Memoized
  List<DbAccessMethodParam> getParams() {
    methodName.params.collect{ parseParam(it) }
  }
}