package nabla2.persistence.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbColumn

trait DbTableTrait<T> {
  abstract List<DbColumn> getColumns()
  abstract String getClassName()
  abstract StrictJavaIdentifier getIdentifier()
  abstract List<DbAccessMethod> getAccessMethods()
  abstract SingleLineText getName()

  @Memoized
  List<DbColumn> getPkColumns() {
    columns.findAll{ it.isPk.truthy }
  }

  @Memoized
  DbAccessMethod accessMethodWithName(String name) {
    accessMethods.find{ it.name.sameAs(name) }.with {
      if (!it) throw new IllegalArgumentException(
        "Unknown name of a access method: ${name}\n${this}"
      )
      it
    }
  }

  /**
   * 自動生成されないアクセスメソッドのリストを返す。
   * @return 自動生成されないアクセスメソッドのリスト
   */
  List<DbAccessMethod> getCustomMethods() {
    accessMethods.findAll{ !it.jpaBacked }
  }

  @Memoized
  boolean hasCompositeKey() {
    pkColumns.size() > 1
  }

  @Memoized
  String getPkClassName() {
    "${entityClassName}.PK"
  }

  @Memoized
  String getIdType() {
    hasCompositeKey() ? pkClassName
                      : pkColumns.first().javaDataType
  }

  @Memoized
  String getEntityClassName() {
    "${className}Entity"
  }

  @Memoized
  String getRepositoryClassName() {
    "${className}Repository"
  }

  @Memoized
  String getTableName() {
    identifier.dasherized.get().replace('-', '_').toLowerCase()
  }
}