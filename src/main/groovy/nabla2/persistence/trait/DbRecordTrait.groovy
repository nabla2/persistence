package nabla2.persistence.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.MultiLineText
import nabla2.persistence.model.DbColumn

trait DbRecordTrait<T> {

  abstract MultiLineText getColumnValue()
  abstract DbColumn getColumn()

  @Memoized
  String getJsonType() {
    String type = column.dataType.get()
    if (type == 'number' || type == 'long') return 'number'
    return 'string'
  }

  String getJsonValue() {
    columnValue.value.map { v ->
      if (jsonType == 'number') return v
      return "\"${v}\""
    }.orElse("null")
  }
}