package nabla2.persistence.trait

import groovy.transform.Immutable

@Immutable
class DbAccessMethodParam {
  String parentKey
  String key
  String name
  String type
  String getPath() {
    parentKey ? (parentKey + '.' + key)
              : key
  }
}
