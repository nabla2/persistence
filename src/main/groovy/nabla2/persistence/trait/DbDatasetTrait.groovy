package nabla2.persistence.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.persistence.model.DbRecord
import nabla2.persistence.model.DbTable

trait DbDatasetTrait<T> {

  abstract List<DbRecord> getRecords()
  abstract StrictJavaIdentifier getIdentifier()

  @Memoized
  Map<DbTable, List<DbRecord>> getRecordsByTable() {
    records.groupBy { it.table }
  }

  String getDatasetName() {
    identifier.dasherized.get().replace('-', '_').toLowerCase()
  }

  @Memoized
  List<DbTable> getTables() {
    recordsByTable.keySet().toList()
  }
}