package nabla2.persistence.trait

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbColumn
import nabla2.persistence.model.DbTable

@Canonical
class DbAccessMethodReturnType {
  DbAccessMethod method
  String key
  String type
  DbTable table
  List<DbAccessMethodReturnType> children

  static DbAccessMethodReturnType of(DbAccessMethod method) {

    String type = method.returnType.value.orElse(
        method.methodId.startsWith('findOne') ? method.mainTable.entityClassName
      : method.methodId.startsWith('find') ? "List<${method.mainTable.entityClassName}>"
      : method.methodId.startsWith('count') ? 'Long'
      : 'List<Tuple>'
    )

    type = type.replaceAll(/【([^】]+)】/) { _, name ->
      DbTable found = method.project.tables.find{ it.name.sameAs(name) }
      if (!found) throw new IllegalStateException("Unknown table:${m.group(1)}\n${method}")
      found.entityClassName
    }

    List<String> descs = method.recordContent.value.map {
      it.tokenize('\n').collect{it.trim()}
    }.orElseGet{
      if (method.scriptType.sameAs('JPQL')) return []
      String tableName = method.mainTable.name.get()
      ["${tableName}:【${tableName}】"]
    }

    if (descs.empty) {
      return new DbAccessMethodReturnType(
        method: method,
        key:'',
        type:type,
        table:null,
        children:[]
      )
    }
    new DbAccessMethodReturnType(
      key: '',
      type: type,
      table: null,
      children: descs.collect {
        List<String> tokens = it.split(':')
        DbTable table = tokens.last().with { tableRef ->
          if (!tableRef.startsWith('【')) return null
          method.project.tables.find{ it.name.value.map{"【${it}】" == tableRef}.orElse(false) }
        }
        new DbAccessMethodReturnType(
          key: tokens.first(),
          type: table ? table.entityClassName : tokens.last(),
          table: table,
          children:[]
        )
      },
    )
  }

  boolean isTuple() {
    type.startsWith('Tuple') || type.contains('<Tuple>')
  }

  boolean isList() {
    type.startsWith('List')
  }

  String retrievalCodeSnippetFor(String itemName, String varRef) {
    if (!itemName) {
      return varRef
    }
    List<String> tokens = itemName.tokenize('.')
    List<String> segments = [list ? 'each' : varRef]
    int foundAt = children.findIndexOf {it.key == tokens.first()}
    if (foundAt < 0) throw new IllegalArgumentException(
      "Unknown member name: ${itemName} of the return type variable: ${method.recordContent}"
    )
    DbAccessMethodReturnType child = children[foundAt]
    if (tuple) {
      segments.add(
        ".get(${foundAt}, ${child.type}.class)"
      )
    }
    if (child.table && tokens.size() > 1) {
      DbColumn column = child.table.columns.find{it.name.sameAs( tokens.last()) }
      if (!column) throw new IllegalArgumentException(
        "Unknown column name ${tokens.last()} of the table ${table.name}"
      )
      segments.add(
        ".get${column.identifier.upperCamelized.get()}()"
      )
    }
    return segments.join('')
  }
}

