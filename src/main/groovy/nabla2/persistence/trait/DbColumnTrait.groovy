package nabla2.persistence.trait

import nabla2.metamodel.datatype.StrictJavaIdentifier

trait DbColumnTrait<T> {

  abstract StrictJavaIdentifier getIdentifier()
  abstract StrictJavaIdentifier getDataType()


  String getColumnId() {
    identifier.dasherized.get().replace('-', '_')
  }

  String getJavaDataType() {
    dataType.upperCamelized.get()
  }
}