package nabla2.gradle

import org.gradle.api.Project
import nabla2.gradle.BasePlugin
import nabla2.persistence.gradle.GenerateDataLoader

/**
 * Persistence自動生成Gradleプラグイン
 *
 * @author nabla2.metamodel.generator
 */
class PersistenceGradlePlugin extends BasePlugin {

  void apply(Project project) {
    register(
      project,
      GenerateDataLoader,
      GenerateDataLoader.Settings,
    )
  }
}